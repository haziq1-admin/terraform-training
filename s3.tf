resource "aws_s3_bucket" "first_bucket" {
    bucket = "zaqih-bucket" // AWS Bucket works like DNS, so it should be unique.
    region = "eu-west-1"    // Provide the region for the S3 Bucket
}